import React, { useState, useEffect } from "react";

//import "./styles.css";

function App() {
    // Declare multiple state variables!
    const [teamA, setTeamA] = useState(0);
    const [teamB, setTeamB] = useState(0);
    useEffect(() => {
        document.title = `You clicked ${teamA} times`
    }, [teamB]) // Only re-run the effect if count of teamB changes

    const handleClick = () => {
        setTeamA(0);
        setTeamB(0);
    }

    // ...
    return (
        <React.Fragment>
            <div className="row ml-4 pl-2">
                <div className="col-4 bg-light text-center">
                    <div className="row">
                        <div className="col"><strong>TeamA</strong></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><h5>{teamA}</h5></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><button className="btn btn-warning" onClick={() => setTeamA(teamA + 3)}>+3 Points</button></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><button className="btn btn-warning" onClick={() => setTeamA(teamA + 2)}>+2 Points</button></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><button className="btn btn-warning" onClick={() => setTeamA(teamA + 1)}>Free Throw</button></div>
                    </div>
                </div>
                <div className="col-4 bg-light text-center">
                    <div className="row">
                        <div className="col"><strong>TeamB</strong></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><h5>{teamB}</h5></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><button className="btn btn-warning" onClick={() => setTeamB(teamB + 3)}>+3 Points</button></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><button className="btn btn-warning" onClick={() => setTeamB(teamB + 2)}>+2 Points</button></div>
                    </div>
                    <div className="row mb-1">
                        <div className="col"><button className="btn btn-warning" onClick={() => setTeamB(teamB + 1)}>Free Throw</button></div>
                    </div>
                </div>
            </div>
            <div className="row ">
                <div className="col-8 text-center">
                    <button className="btn btn-warning" onClick={() => handleClick()}>Reset</button>
                </div>
            </div>
        </React.Fragment>

    )
}

export default App;