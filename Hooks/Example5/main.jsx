import React, { createContext, useContext, useState } from "react";
import { render } from "react-dom";

const TitleContext = createContext(); //context object
// It returns an object with 2 values:
// { Provider, Consumer }

const UiComponent = props => {
    const [count, setCount] = useState(0);
    let title = useContext(TitleContext);

    return (
        <React.Fragment>
            <div className="row">
                <div className="col ml-2">
                    <strong>UiComponent: </strong>
                    {title}
                </div>
            </div>
            <div className="row">
                <div className="col-6 text-center"><h6>Clicked :: {count} times</h6></div>
                <div className="col-6 text-left">
                    <button className="btn btn-warning" onClick={() => setCount(count + 1)}>Click Me!</button>
                </div>
            </div>
        </React.Fragment>

    );
};

const App = props => {
    return (
        <TitleContext.Provider value="Value that gets passed down">
            <UiComponent />
        </TitleContext.Provider>
    );
};

export default App;