import React, { useState } from "react";

//import "./styles.css";

function App() {
    // Declare a new state variable, which we'll call "count"
    const [count, setCount] = useState(0);

    return (
        <div className="App">
            <h1>Counter Demo</h1>
            <h2>You clicked {count} times!</h2>
            <button className="btn btn-primary mr-2" onClick={() => setCount(count - 1)} disabled={count === 0}>Decrement</button>
            <button className="btn btn-primary" onClick={() => setCount(count + 1)}>Increment</button>
        </div>
    );
}

export default App;