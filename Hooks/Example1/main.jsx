import React, { useState } from "react";

//import "./styles.css";

function App() {
    // Declare  state variable!
    let dummylist = [
        { name: "Perk", quantity: 10, sales: 7 },
        { name: "5Star", quantity: 7, sales: 9 },
        { name: "Pepsi", quantity: 10, sales: 20 },
        { name: "Maggi", quantity: 41, sales: 22 },
        { name: "Coke", quantity: 18, sales: 50 }
    ];
    const [products, setProducts] = useState(dummylist);
    // ...
    return (
        <React.Fragment>
            <h3>Details :</h3>
            <ul>
                {products.map(obj =>
                    (
                        <li>Name :{obj.name}, quantity :{obj.quantity}</li>
                    ))}
            </ul>
        </React.Fragment>

    )
}

export default App;