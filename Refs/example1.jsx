import React, { Component } from "react";

class Main extends Component {
    constructor(props) {
        super(props);
        //creating ref named myRef
        this.myRef = React.createRef();
    }


    componentDidMount = () => {
        // myRef only has a current property
        console.log(this.myRef);
        // myRef.current is what we are interested in
        console.log(this.myRef.current);
        // focus our input automatically when component mounts
        this.myRef.current.focus();
    }


    handleClick = () => {
        //accessing myRef 
        this.myRef.current.focus();
    }


    render() {
        return (
            <React.Fragment>
                <label for="email">Email:</label>
                <input
                    name="email"
                    onChange={this.onChange}
                    ref={this.myRef}
                    type="text" />
                <br />
                <button className="btn btn-primary" onClick={this.handleClick}>Focus Email Input </button>

            </React.Fragment>
        )
    }
}

export default Main;