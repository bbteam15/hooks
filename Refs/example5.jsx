import React, { Component } from 'react';


const EmailInput = React.forwardRef((props, ref) => (
    <input ref={ref} {...props} type="email" className="form-control" />
));

class Main extends Component {
    emailRef = React.createRef();

    render() {
        return (
            <div>
                <h4>Forward Ref example</h4>
                <div className="row">
                    <div className="col-5">
                        <EmailInput ref={this.emailRef} />
                    </div>

                    <div className="col-5 text-center">
                        <button className="btn btn-danger" onClick={() => this.onClickButton()}>Click me to focus email</button>
                    </div>
                </div>

            </div>
        );
    }

    // `this.emailRef.current` points to the `input` component inside of EmailInput,
    // because EmailInput is forwarding its ref via the `React.forwardRef` callback.
    onClickButton() {
        this.emailRef.current.focus();
    }
}

export default Main;