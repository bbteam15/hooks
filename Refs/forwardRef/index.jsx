import React from "react";
import Hello from "./Hello";
import CustomText from "./Ref";

const styles = {
    fontFamily: "sans-serif",
    textAlign: "center"
};

const Main = () => (
    <div style={styles}>
        <Hello name="React Refs" />
        <CustomText />
    </div>
);
export default Main;