import React, { Component } from 'react';


class Main extends React.Component {
    constructor(props) {
        super(props);
        this.callRef = React.createRef();
        this.addingRefInput = this.addingRefInput.bind(this);
    }

    addingRefInput = () => {
        this.callRef.current.focus();
    }

    render() {
        return (
            <div>
                <h2>Adding Ref to DOM element</h2>
                <input
                    type="text"
                    ref={this.callRef} />
                <button
                    className="btn btn-warning"
                    onClick={this.addingRefInput}
                >Add Text Input</button>
            </div>
        );
    }
}
export default Main;  