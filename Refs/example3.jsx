import React, { Component } from 'react';

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.callRefInput = null;

        //setting ref
        this.setInputRef = element => {
            this.callRefInput = element;
        };

        this.focusRefInput = () => {
            //Focus the input using the raw DOM API  
            if (this.callRefInput) this.callRefInput.focus();
        };
    }

    componentDidMount() {
        //autofocus of the input on mount  
        this.focusRefInput();
    }

    render() {
        return (
            <div>
                <h2>Callback Refs Example</h2>
                <input
                    type="text"
                    ref={this.setInputRef}
                />
                <button className="btn btn-warning" onClick={this.focusRefInput}>Focus Input Text</button>

            </div>
        );
    }
}
export default Main; 