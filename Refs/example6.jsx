import React, { useState, useRef } from "react";


//import "./styles.css";

function Main() {
    let [name, setName] = useState("Edufect");

    let nameRef = useRef();

    const submitButton = () => {
        console.log(nameRef.current.value)
        setName(nameRef.current.value);
    };


    return (

        <div className="App">
            <h4>{name}</h4>

            <div>
                <input ref={nameRef} type="text" />
                <button className="btn btn-warning" onClick={submitButton}>
                    Submit
        </button>
            </div>
        </div>
    );
}

export default Main;