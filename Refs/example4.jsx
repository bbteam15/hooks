import React, { Component } from 'react';
import { render } from 'react-dom';

const TextInput = React.forwardRef((props, ref) => (
    <input type="text" placeholder="Hello World" ref={ref} />
));

const inputRef = React.createRef();

class Main extends React.Component {
    handleSubmit = e => {
        e.preventDefault();
        console.log(inputRef.current.value);
    };
    render() {
        return (
            <div>
                <form onSubmit={e => this.handleSubmit(e)}>
                    <TextInput ref={inputRef} />
                    <button>Submit</button>
                </form>
            </div>
        );
    }
}
export default Main; 